from abc import ABC, abstractmethod
class Person(ABC):
    
    #methods  
    @abstractmethod
    def get_full_name(self):
      pass
    @abstractmethod
    def add_request(self):
     pass
    @abstractmethod
    def check_request(self):
      pass
    @abstractmethod
    def add_user(self, person):
      pass
    @abstractmethod
    def login(self):
      pass
    @abstractmethod
    def logout(self):
      pass

#Employee class
class Employee(Person):
  def __init__(self, first_name, last_name,email,department):
    super().__init__()
    self._first_name = first_name
    self._last_name = last_name
    self._email = email
    self._department = department
  #getters and setters
  def get_first_name(self):
    return self._first_name
  def set_first_name(self, first_name):
    self._first_name = first_name

  def get_last_name(self):
    return self._last_name
  def set_last_name(self, last_name):
    self._last_name = last_name

  def get_email(self):
    return self._email
  def set_email(self, email):
    self._email = email

  def get_department(self):
    return self._department
  def set_department(self, department):
    self._department = department
  
  def get_full_name(self):
    return f"{self.get_first_name()} {self.get_last_name()}"
  def add_request(self):
    return f"Request has been added"
  def check_request(self):
    return f"Request has been checked"
  def add_user(self):
    return f"User has been added"
  def login(self):
    return f"{self.get_email()} has logged in"
  def logout(self):
   return f"{self.get_email()} has logged out"

class Team_Lead(Person):
  def __init__(self, first_name, last_name,email,department):
    super().__init__()
    self._first_name = first_name
    self._last_name = last_name
    self._email = email
    self._department = department
    self._members = []
  #getters and setters
  def get_first_name(self):
    return self._first_name
  def set_first_name(self, first_name):
    self._first_name = first_name

  def get_last_name(self):
    return self._last_name
  def set_last_name(self, last_name):
    self._last_name = last_name

  def get_email(self):
    return self._email
  def set_email(self, email):
    self._email = email

  def get_department(self):
    return self._department
  def set_department(self, department):
    self._department = department
  
  def get_members(self):
    return self._members
  
  
  def get_full_name(self):
    return f"{self.get_first_name()} {self.get_last_name()}"
  def add_request(self):
    return f"Request has been added"
  def check_request(self):
    return f"Request has been checked"
  def add_user(self):
    return f"User has been added"
  def login(self):
    return f"{self.get_email()} has logged in"
  def logout(self):
    return f"{self.get_email()} has logged out"
  def add_member(self, member):
    self._members.append(member)

class Admin(Person):
  def __init__(self, first_name, last_name,email,department):
    super().__init__()
    self._first_name = first_name
    self._last_name = last_name
    self._email = email
    self._department = department
  #getters and setters
  def get_first_name(self):
    return self._first_name
  def set_first_name(self, first_name):
    self._first_name = first_name

  def get_last_name(self):
    return self._last_name
  def set_last_name(self, last_name):
    self._last_name = last_name

  def get_email(self):
    return self._email
  def set_email(self, email):
    self._email = email

  def get_department(self):
    return self._department
  def set_department(self, department):
    self._department = department
  
  def get_full_name(self):
    return f"{self.get_first_name()} {self.get_last_name()}"
  def add_request(self):
    return f"Request has been added"
  def check_request(self):
    return f"Request has been checked"
  def add_user(self):
    return f"User has been added"
  def login(self):
    return f"{self.get_email()} has logged in"
  def logout(self):
   return f"{self.get_email()} has logged out"
  
class Request():
  def __init__(self, name, requester, dateRequested):
    self._name = name
    self._requester = requester
    self._dateRequested = dateRequested
    self._status = "Pending"
    
  def get_name(self):
    return self._name
  def set_name(self, name):
    self._name = name
  def get_requester(self):
    return self._requester
  def set_requester(self, requester):
    self._requester = requester
  def get_dateRequested(self):
    return self._dateRequested
  def set_dateRequested(self, dateRequested):
    self._dateRequested = dateRequested
  def get_status(self):
    return self._status
  def set_status(self, status):
    self._status = status
  def update_request(self):
    return f"Request {self.get_name()} has been updated"
  def close_request(self):
    return f"Request {self.get_name()} has been closed"
  def delete_request(self):
    return f"Request {self.get_name()} has been deleted"

def test_cases():
  employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
  employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
  employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
  employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
  admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
  teamLead1 = Team_Lead("Michael", "Specter", "smichael@mail.com", "Sales")
  req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
  req2 = Request("Laptop repair", employee1, "1-Jul-2021")

  assert employee1.get_full_name() == "John Doe", "Full name should be John Doe"
  assert admin1.get_full_name() == "Monika Justin", "Full name should be Monika Justin"
  assert teamLead1.get_full_name() == "Michael Specter", "Full name should be Michael Specter"
  assert employee2.login() == "sjane@mail.com has logged in"
  assert employee2.add_request() == "Request has been added"
  assert employee2.logout() == "sjane@mail.com has logged out"

  teamLead1.add_member(employee3)
  teamLead1.add_member(employee4)
  for indiv_emp in teamLead1.get_members():
      print(indiv_emp.get_full_name())

  assert admin1.add_user() == "User has been added"

  req2.set_status("closed")
  print(req2.close_request())

test_cases()